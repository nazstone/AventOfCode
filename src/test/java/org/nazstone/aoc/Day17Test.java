package org.nazstone.aoc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.nazstone.aoc.day17.Day17;
import org.nazstone.aoc.day17.Node;
import org.nazstone.aoc.util.Util;

import java.util.*;

class Day17Test {
    @Test
    public void inputTest() {
        Day17 day17 = new Day17();
        String[][] grid = Util.fromFileToGrid("day17/input");
        Node node = day17.createNode(grid, 0, 0);

        Assertions.assertEquals(node.getNeighbors().size(), 2);
        Assertions.assertEquals(day17.getNodes().values().size(), 169);
    }
    @Test
    public void inputXWrongTest() {
        Day17 day17 = new Day17();
        String[][] grid = Util.fromFileToGrid("day17/input");
        Node node = day17.createNode(grid, -1, 0);
        Assertions.assertNull(node);
    }
    @Test
    public void inputYWrongTest() {
        Day17 day17 = new Day17();
        String[][] grid = Util.fromFileToGrid("day17/input");
        Node node = day17.createNode(grid, 0, -1);
        Assertions.assertNull(node);
    }

    @Test
    public void calculateTest() {
        Day17 day17 = new Day17();
        String[][] grid = Util.fromFileToGrid("day17/input");
        day17.createNode(grid, 0, 0);
        List<Node> path = day17.calculateShortestPath("0-0", "12-12");
        day17.print(path);
    }
}
