package org.nazstone.aoc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nazstone.aoc.day19.Condition;
import org.nazstone.aoc.day19.Day19;
import org.nazstone.aoc.day19.Workflow;
import org.nazstone.aoc.util.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

class Day19Test {
  @Test
  void loadDataTest() {
    Day19 day19 = new Day19();
    String content = Util.fromFileLineToString("day19/input");
    String[] subcontent = content.split("\\r?\\n\\r?\\n");
    day19.initWorkflow(subcontent[0]);
    Assertions.assertNotNull(day19.getWorkflows());
    Assertions.assertEquals("px", day19.getWorkflows().get("px").getName());

    day19.initRating(subcontent[1]);
    Assertions.assertNotNull(day19.getInputs());
    Assertions.assertEquals(787, day19.getInputs().get(0).get("x"));

    Assertions.assertEquals(19114, day19.letsgo());
  }

  @Test
  void nextStepTest() {
    Day19 day19 = new Day19();
    Workflow workflow = Workflow.builder()
            .name("in")
            .output("qqz")
            .conditions(Collections.singletonList(Condition.builder()
                    .val(1351)
                    .cond("<")
                    .property("s")
                    .output("px")
                    .build())).build();
    String next = day19.nextStep(workflow, Map.of("x", 787, "m", 2655, "a", 1222, "s", 2876));
    Assertions.assertEquals("qqz", next);
  }
}
