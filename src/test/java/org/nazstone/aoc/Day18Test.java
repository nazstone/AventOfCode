package org.nazstone.aoc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nazstone.aoc.day18.Day18;
import org.nazstone.aoc.day18.Input;

import java.util.List;

class Day18Test {
  @Test
  void testWidth() {
    Day18 day18 = new Day18();
    List<Input> inputs = day18.getInputs();
    double area = day18.getDimEtc(inputs);
    Assertions.assertEquals(62, area);
  }
  @Test
  void testWidth2() {
    Day18 day18 = new Day18();
    List<Input> inputs = day18.getInputs2();
    double area = day18.getDimEtc(inputs);
    Assertions.assertEquals(952408144115l, area);
  }
  // 952408144115
  // 1072888051
}
