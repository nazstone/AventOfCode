package org.nazstone.aoc;

import org.javatuples.Quartet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nazstone.aoc.day16.Day16;
import org.nazstone.aoc.day16.Position;
import org.nazstone.aoc.util.Util;

public class Day16Test {
    @Test
    public void calculNextPositionGoDownTest() {
        Day16 day16 = new Day16();
        String[][] grid = new String[2][2];
        // *\
        // ..
        grid[0][0] = ".";
        grid[1][0] = "\\";
        grid[0][1] = ".";
        grid[1][1] = ".";
        day16.setGrid(grid);
        day16.setGridTile(grid);
        Position position = Position.builder().x(0).y(0).build();
        Position direction = Position.builder().x(1).y(0).build();

        Quartet<Position, Position, Position, Position> newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 1 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == 1 && direction.getY() == 0);
        Assertions.assertEquals(day16.countTile(), 1);
        Assertions.assertNull(newPosTriplet.getValue2());

        newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 1 && position.getY() == 1);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == 1);
        Assertions.assertEquals(day16.countTile(), 2);
        
        Assertions.assertNull(newPosTriplet.getValue2());
    }
    @Test
    public void calculNextPositionGoDown2Test() {
        Day16 day16 = new Day16();
        String[][] grid = new String[2][2];
        grid[0][0] = ".";
        grid[1][0] = ".";
        grid[0][1] = "\\";
        grid[1][1] = ".";
        day16.setGrid(grid);
        day16.setGridTile(grid);
        Position position = Position.builder().x(1).y(1).build();
        Position direction = Position.builder().x(-1).y(0).build();

        Quartet<Position, Position, Position, Position> newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 1);
        Assertions.assertTrue(direction.getX() == -1 && direction.getY() == 0);
        Assertions.assertEquals(day16.countTile(), 1);
        Assertions.assertNull(newPosTriplet.getValue2());

        newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == -1);
        Assertions.assertEquals(day16.countTile(), 2);
        Assertions.assertNull(newPosTriplet.getValue2());
    }

    @Test
    public void calculNextPositionGoUpTest() {
        Day16 day16 = new Day16();
        String[][] grid = new String[2][2];
        grid[0][0] = ".";
        grid[1][0] = ".";
        grid[0][1] = "\\";
        grid[1][1] = ".";
        day16.setGrid(grid);
        day16.setGridTile(grid);
        Position position = Position.builder().x(1).y(1).build();
        Position direction = Position.builder().x(-1).y(0).build();

        Quartet<Position, Position, Position, Position> newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 1);
        Assertions.assertTrue(direction.getX() == -1 && direction.getY() == 0);
        Assertions.assertEquals(day16.countTile(), 1);
        Assertions.assertNull(newPosTriplet.getValue2());

        newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == -1);
        Assertions.assertEquals(day16.countTile(), 2);
        Assertions.assertNull(newPosTriplet.getValue2());
    }

    @Test
    public void calculNextPositionGoUp2Test() {
        Day16 day16 = new Day16();
        String[][] grid = new String[2][2];
        grid[0][0] = "/";
        grid[1][0] = ".";
        grid[0][1] = ".";
        grid[1][1] = ".";
        day16.setGrid(grid);
        day16.setGridTile(grid);
        Position position = Position.builder().x(1).y(0).build();
        Position direction = Position.builder().x(-1).y(0).build();

        Quartet<Position, Position, Position, Position> newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == -1 && direction.getY() == 0);
        Assertions.assertEquals(day16.countTile(), 1);

        newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 1);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == 1);
        Assertions.assertEquals(day16.countTile(), 2);
    }

    @Test
    public void calculNextPositionGoPipeLeftTest() {
        Day16 day16 = new Day16();
        String[][] grid = new String[3][3];
        grid[0][0] = ".";
        grid[1][0] = ".";
        grid[0][1] = ".";
        grid[1][1] = "|";
        grid[2][0] = ".";
        grid[2][1] = ".";
        grid[2][2] = ".";
        // ...
        // .|.
        // ...
        day16.setGrid(grid);
        day16.setGridTile(grid);
        Position position = Position.builder().x(0).y(1).build();
        Position direction = Position.builder().x(1).y(0).build();

        Quartet<Position, Position, Position, Position> newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 1 && position.getY() == 1);
        Assertions.assertTrue(direction.getX() == 1 && direction.getY() == 0);
        Assertions.assertEquals(day16.countTile(), 1);

        newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 1 && position.getY() == 2);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == 1);

        position = newPosTriplet.getValue2();
        direction = newPosTriplet.getValue3();
        Assertions.assertTrue(position.getX() == 1 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == -1);

        Assertions.assertEquals(day16.countTile(), 2);
    }

    @Test
    public void calculNextPositionGoMinusLeftTest() {
        Day16 day16 = new Day16();
        String[][] grid = new String[3][3];
        grid[0][0] = ".";
        grid[1][0] = "-";
        grid[2][0] = ".";
        grid[0][1] = ".";
        grid[1][1] = ".";
        grid[2][1] = ".";
        // .-.
        // .*.
        day16.setGrid(grid);
        day16.setGridTile(grid);
        Position position = Position.builder().x(1).y(1).build();
        Position direction = Position.builder().x(0).y(-1).build();

        Quartet<Position, Position, Position, Position> newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 1 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == 0 && direction.getY() == -1);
        Assertions.assertEquals(day16.countTile(), 1);

        newPosTriplet = day16.calculateNewPosition(position, direction);
        position = newPosTriplet.getValue0();
        direction = newPosTriplet.getValue1();
        Assertions.assertTrue(position.getX() == 0 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == -1 && direction.getY() == 0);

        position = newPosTriplet.getValue2();
        direction = newPosTriplet.getValue3();
        Assertions.assertTrue(position.getX() == 2 && position.getY() == 0);
        Assertions.assertTrue(direction.getX() == 1 && direction.getY() == 0);

        Assertions.assertEquals(day16.countTile(), 2);
    }


    @Test
    public void inputTest() {
        Day16 day16 = new Day16();
        String[][] grid = Util.fromFileToGrid("day16/input");
        String[][] gridTile = new String[grid.length][grid[0].length];

        day16.setGrid(grid);
        day16.setGridTile(gridTile);

        Position actualPosition = Position.builder().x(0).y(0).build();
        Position direction = Position.builder().x(1).y(0).build();

        day16.letsgo(actualPosition, direction);

        int count = day16.countTile();
        day16.logGrid();
        Assertions.assertEquals(count, 46);
    }@Test
    public void inputPart2Test() {
        Day16 day16 = new Day16();
        String[][] grid = Util.fromFileToGrid("day16/input");
        day16.setGrid(grid);
        day16.setGridTile(new String[grid.length][grid[0].length]);

        int count = day16.part2();
        Assertions.assertEquals(count, 51);
    }
}
