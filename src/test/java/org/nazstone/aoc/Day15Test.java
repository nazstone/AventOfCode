package org.nazstone.aoc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nazstone.aoc.day15.Day15;
import org.nazstone.aoc.day15.Lens;
import org.nazstone.aoc.util.Util;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Day15Test {
    private Day15 day15 = new Day15();
    @Test
    void hashTest1() {
        Assertions.assertEquals(day15.hash("H"), 200);
        Assertions.assertEquals(day15.hash("HASH"), 52);
    }
    @Test
    void hashTest2() {
        Assertions.assertEquals(day15.hash("rn=1"), 30);
        Assertions.assertEquals(day15.hash("cm-"), 253);
        Assertions.assertEquals(day15.hash("qp=3"), 97);
        Assertions.assertEquals(day15.hash("cm=2"), 47);
        Assertions.assertEquals(day15.hash("qp-"), 14);
    }

    @Test
    void sumHashTest() {
        List<String> list = Util.fromFileOneLineCommaToStringList("day15/input");
        int val = day15.sumHash(list);
        Assertions.assertEquals(val, 1320);
    }

    @Test
    void fillTest() {
        List<String> list = Util.fromFileOneLineCommaToStringList("day15/input");
        Map<Integer, List<Lens>> map = day15.fillBox(list);
        Assertions.assertEquals(map.get(0).size(), 2);
        Assertions.assertEquals(map.get(3).size(), 3);
        Assertions.assertEquals(map.get(1).size(), 0);

        // box 3 last max lens "pc"
        Optional<Lens> elt = map.get(3).stream().max((a, b) -> a.getSort() - b.getSort());
        if (elt.isEmpty()) {
            Assertions.fail();
        }
        Assertions.assertEquals(elt.get().getLabel(), "pc");

        elt = map.get(3).stream().min((a, b) -> a.getSort() - b.getSort());
        if (elt.isEmpty()) {
            Assertions.fail();
        }
        Assertions.assertEquals(elt.get().getLabel(), "ot");
    }

    @Test
    void sumBoxTest() {
        List<String> list = Util.fromFileOneLineCommaToStringList("day15/input");
        Map<Integer, List<Lens>> map = day15.fillBox(list);
        Assertions.assertEquals(day15.calculateForBox(map), 145);
    }
}
