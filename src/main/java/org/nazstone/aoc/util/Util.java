package org.nazstone.aoc.util;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class Util {
    private Util() {}
    @SneakyThrows
    public static List<String> fromFileLineToStringList(String path) {
        String input = Files.readString(Path.of(ClassLoader.getSystemResource(path).toURI()));

        String[] linesTmp = input.split("\\r?\\n");
        log.debug("file size: {}", linesTmp.length);
        return Arrays.asList(linesTmp);
    }
    @SneakyThrows
    public static String fromFileLineToString(String path) {
        return Files.readString(Path.of(ClassLoader.getSystemResource(path).toURI()));
    }
    @SneakyThrows
    public static List<String> fromFileOneLineCommaToStringList(String path) {
        String input = Files.readString(Path.of(ClassLoader.getSystemResource(path).toURI()));

        String[] linesTmp = input.trim().split(",");
        log.debug("file size: {}", linesTmp.length);
        return Arrays.asList(linesTmp);
    }

    @SneakyThrows
    public static String[][] fromFileToGrid(String path) {

        String input = Files.readString(Path.of(ClassLoader.getSystemResource(path).toURI()));
        String[] linesTmp = input.split("\\r?\\n");
        String[][] grid = new String[linesTmp[0].length()][linesTmp.length];
        for (int y = 0; y < linesTmp.length; y++) {
            String[] charas = linesTmp[y].split("");
            for (int x = 0; x < charas.length; x++) {
                grid[x][y] = charas[x];
            }
        }
        return grid;
    }
}
