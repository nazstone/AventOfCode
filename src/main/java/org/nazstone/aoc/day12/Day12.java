package org.nazstone.aoc.day12;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class Day12 {
    private int length = 5;
    @SneakyThrows
    public void execute() {
        log.info("Day 12");

        List<String> lines = Util.fromFileLineToStringList("./day12/input");

        long count = 0;
        int j = 0;
        for (String line : lines) {
            String[] lineSplit = line.split("\\s");

            String input = lineSplit[0];
            String verify = lineSplit[1];
            String inputTmp = "";
            String verifyTmp = "";

            for (int i = 0; i < length; i += 1) {
                inputTmp += input;
                verifyTmp += verify;
                if (i < length - 1) {
                    verifyTmp += ",";
                }
            }

            List<String> inputSamples = generateList(inputTmp, verifyTmp);
            for (String inputSample : inputSamples) {
                if (checkValue(inputSample, Arrays.stream(verifyTmp.split(",")).map(Integer::parseInt).toList())) {
                    count += 1;
                }
            }

            log.debug("Iteration: {} - count: {}", j, count);
            j++;
        }
        log.debug("Count: {}", count);
    }

    private List<String> generateList(String input, String verify) {
        long inputQuestion = input.chars().filter(e -> e == '?').count();
        long countMark = input.chars().filter(e -> e == '#').count();

        int count = Arrays.stream(verify.split(",")).map(Integer::parseInt).reduce(Integer::sum).orElse(0);

        List<String> list = new ArrayList<>();

        double maxCombine = Math.pow(2, inputQuestion);
        for (long i = 0; i < maxCombine; i += 1) {
            String str = Long.toBinaryString(i);
            long countOne = str.chars().filter(e -> e == '1').count();

            if (countMark + countOne != count) {
                continue;
            }

            if (str.length() < inputQuestion) {
                str = String.format("%1$" + inputQuestion + "s", str).replace(' ', '0');
            }

            String copyInputReplace = input;

            String[]  chara = str.split("");
            for (String s : chara) {
                copyInputReplace = copyInputReplace.replaceFirst("\\?", s.equals("0") ? "." : "#");
            }

            list.add(copyInputReplace);
        }

        return list;
    }

    private boolean checkValue(String s, List<Integer> toList) {
        List<Integer> toListClone = new ArrayList<>(toList);
        Pattern pattern = Pattern.compile("(#*)");
        Matcher r = pattern.matcher(s);

        while (r.find()) {
            if (r.group().length() == 0) {
                continue;
            }
            if (toListClone.isEmpty()) {
                return false;
            }
            if (toListClone.get(0) != r.group().length()) {
                return false;
            }
            toListClone.remove(0);
        }
        return toListClone.isEmpty();
    }
}
