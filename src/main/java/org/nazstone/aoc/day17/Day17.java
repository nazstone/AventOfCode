package org.nazstone.aoc.day17;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.nazstone.aoc.util.Util;

import java.util.*;

@Getter
@Slf4j
public class Day17 {
    private final Map<String, Node> nodes = new HashMap<>();
    @Setter
    private String[][] grid;

    @SneakyThrows
    public void execute() {
        log.info("Day 17");

        createNode(Util.fromFileToGrid("day17/input"), 0, 0);
    }

    public String getName(int x, int y) {
        return String.format("%d-%d", x, y);
    }

    public String getName(Node node) {
        return getName(node.getX(), node.getY());
    }

    public Pair<Node, Integer> findSmallestHeat(Map<Node, Integer> frontiers) {
        Optional<Map.Entry<Node, Integer>> val = frontiers.entrySet().stream()
                .min(Comparator.comparingInt(Map.Entry::getValue));
        Node tmp = val.get().getKey();
        int heat = val.get().getValue();
        frontiers.remove(val.get().getKey());
        return Pair.with(tmp, heat);
    }

    public List<Node> calculateShortestPath(String from, String to) {
        Node nodeOrigin = nodes.get(from);

        Set<String> explored = new HashSet<>();
        Map<Node, Node> previous = new HashMap<>();
        Map<Node, Integer> frontiers = new HashMap<>();
        List<Node> path = null;

        frontiers.put(nodeOrigin, 0);

        Node best;
        int bestHeat = 0;

        while (!frontiers.isEmpty()) {
            Pair<Node, Integer> tmp = findSmallestHeat(frontiers);
            best = tmp.getValue0();
            bestHeat = tmp.getValue1();

            String name = getName(best);
            if (name.equals(to)) {
                path = getPath(best, previous);
                break;
            }

            explored.add(name);

            for (Node neighbor : best.getNeighbors()) {
                if (explored.contains(getName(neighbor))) {
                    continue;
                }

                int cumul = bestHeat + neighbor.getHeat();
                List<Node> pathTmp = getPath(best, previous, true);
                boolean sameDirection = false;
                if (pathTmp.size() == 3) {
                    boolean sameX = pathTmp.get(0).getX() == pathTmp.get(1).getX() && pathTmp.get(1).getX() == pathTmp.get(2).getX();
                    boolean sameY = pathTmp.get(0).getY() == pathTmp.get(1).getY() && pathTmp.get(1).getY() == pathTmp.get(2).getY();
                    sameDirection = sameX || sameY;
                }
                if ((!frontiers.containsKey(neighbor) || cumul < frontiers.get(neighbor)) && !sameDirection){
                    previous.put(neighbor, best);
                    frontiers.put(neighbor, cumul);
                }
            }
        }

        log.debug("Best heat: {}", bestHeat);

        return path;
    }

    private List<Node> getPath(Node best, Map<Node, Node> previous) {
        return getPath(best, previous, false);
    }
    
    private List<Node> getPath(Node best, Map<Node, Node> previous, boolean maxThree) {
        List<Node> path = new ArrayList<>();
        Node step = best;
        path.add(step);

        int count = 3;

        while (previous.containsKey(step)) {
            step = previous.get(step);
            path.add(step);
            count -= 1;
            if (maxThree && count == 1) {
                break;
            }
        }
        
        Collections.reverse(path);
        return path;
    }

    public Node createNode(String[][] grid, int x, int y) {
        this.grid = grid;
        // check coord are ok
        if (x >= grid.length || y >= grid[0].length || x < 0 || y < 0) {
            return null;
        }

        // check that the node don't already exists
        String name = getName(x, y);
        if (nodes.containsKey(name)) {
            return nodes.get(name);
        }

        Node node = Node.builder()
                .x(x)
                .y(y)
                .heat(Integer.parseInt(grid[x][y]))
                .neighbors(new ArrayList<>())
                .build();

        // add node in cache
        nodes.put(name, node);

        // add x neigboor
        for (int xTmp = -1; xTmp <= 1; xTmp += 1) {
            if (xTmp == 0) {
                continue;
            }
            Node nodeTmp = createNode(grid, x + xTmp, y);
            if (nodeTmp != null) {
                node.getNeighbors().add(nodeTmp);
            }
        }
        // add y neigboor
        for (int yTmp = -1; yTmp <= 1; yTmp += 1) {
            if (yTmp == 0) {
                continue;
            }
            Node nodeTmp = createNode(grid, x, y + yTmp);
            if (nodeTmp != null) {
                node.getNeighbors().add(nodeTmp);
            }
        }
        return node;
    }

    public void print(List<Node> best) {
        for (int y = 0; y < grid[0].length; y++) {
            for (int x = 0; x < grid.length; x++) {
                if (isCoordInBestPath(best, x, y)) {
                    System.out.print("x");
                } else {
                    System.out.print(grid[x][y]);
                }
            }
            System.out.println();
        }
    }

    private boolean isCoordInBestPath(List<Node> best, int x, int y) {
        return best.stream().anyMatch(e -> e.getY() == y && e.getX() == x);
    }
}
