package org.nazstone.aoc.day17;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@ToString(of = {"x", "y"})
@EqualsAndHashCode(of = {"x", "y"})
public class Node {
    private int x;
    private int y;
    private int heat;
    private List<Node> neighbors;

}
