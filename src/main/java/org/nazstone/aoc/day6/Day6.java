package org.nazstone.aoc.day6;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class Day6 {
    @SneakyThrows
    public void execute() {
        log.info("Day 6");

        List<String> lines = Util.fromFileLineToStringList("./day6/input");
        String s = lines.get(0);
        List<Long> times = Arrays.stream(s.substring(s.indexOf(":") + 1).trim().split("\\s+")).map(Long::valueOf).toList();
        s = lines.get(1);
        List<Long> distances = Arrays.stream(s.substring(s.indexOf(":") + 1).trim().split("\\s+")).map(Long::valueOf).toList();

        log.debug("Time: {}", times);
        log.debug("Distance: {}", distances);

        int compute = 1;

        for (int i = 0; i < times.size(); i += 1) {
            compute = computeHowManyWayToWin(times.get(i), distances.get(i), compute);
        }

        log.info("result1: {}", compute);

        s = lines.get(0);
        Long time = Long.parseLong(s.substring(s.indexOf(":") + 1).trim().replaceAll("\\s+", ""));
        s = lines.get(1);
        Long distance = Long.parseLong(s.substring(s.indexOf(":") + 1).trim().replaceAll("\\s+", ""));

        compute = computeHowManyWayToWin(time, distance, 1);

        log.info("result12 {}", compute);
    }

    private static int computeHowManyWayToWin(Long time, Long distance, int compute) {
        int count = 0;
        for (int j = 1; j < time; j += 1) {
            int holdTime = j;

            if ((time - holdTime) * holdTime > distance) {
                count += 1;
            } else if (count > 0) {
                break;
            }
        }

        compute *= count;
        log.debug("{}", count);
        return compute;
    }
}
