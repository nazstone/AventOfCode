package org.nazstone.aoc.day2;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Result {
    private Integer id;
    private Boolean ok;
    private Integer calcul2;
}