package org.nazstone.aoc.day2;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class Day2 {
    private final Map<String, Integer> maxCubePerColor = Map.of("red", 12, "green", 13, "blue", 14);

    @SneakyThrows
    public void execute() {
        log.info("Day 2");
        List<String> lines = Util.fromFileLineToStringList("./day1/input");

        List<Result> stream = lines.stream()
                .map(s -> {
                    int indexTwoPoint = s.indexOf(":");
                    String id = s.substring("Game ".length(), indexTwoPoint);
                    String[] gamesSet = s.substring(indexTwoPoint + 1).split(";");
                    return lineCalcul(id, gamesSet);
                }).toList();
        int result1 = stream.stream()
                .filter(Result::getOk)
                .map(Result::getId)
                .reduce(Integer::sum)
                .orElse(0);

        log.info("result1 = {}", result1);

        int result2 = stream.stream()
                .map(Result::getCalcul2)
                .reduce(Integer::sum)
                .orElse(0);
        log.info("result2 = {}", result2);
    }

    private Result lineCalcul(String id, String[] gamesSet) {
        boolean ok = true;

        Map<String, Integer> mapResult = new HashMap<>();
        mapResult.put("red", 0);
        mapResult.put("blue", 0);
        mapResult.put("green", 0);

        for (String set : gamesSet) {
            String[] colorsAndNumber = set.split(",");
            for (String colorAndNumber : colorsAndNumber) {
                String[] colorAndNumbersplit = colorAndNumber.trim().split(" ");
                if (colorAndNumbersplit.length > 2) {
                    throw new RuntimeException("number of color is not as expected");
                }
                String countColor = colorAndNumbersplit[0].trim();
                String color = colorAndNumbersplit[1].trim();
                ok &= maxCubePerColor.get(color) >= Integer.parseInt(countColor);

                mapResult.put(color, Math.max(mapResult.get(color), Integer.parseInt(countColor)));
            }
        }
        int result2 = mapResult.values().stream().reduce((a, b) -> a * b).orElse(0);
        return Result.builder().id(Integer.parseInt(id)).ok(ok).calcul2(result2).build();
    }
}
