package org.nazstone.aoc.day1;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.List;
import java.util.Map;

@Slf4j
public class Day1 {
  private final Map<String, String> mapNumber = Map.of(
          "one", "one1one",
          "two", "two2two",
          "three", "three3three",
          "four", "four4four",
          "five", "five5five",
          "six", "six6six",
          "seven", "seven7seven",
          "eight", "eight8eight",
          "nine", "nine9nine"
  );

  private String transformLetterToNumber(String s) {
    // transform letter number to something easier to integrate
    log.debug("input line before change: {}", s);
    for (Map.Entry<String, String> key : mapNumber.entrySet()) {
      s = s.replaceAll(key.getKey(), key.getValue());
    }
    return s.trim();
  }

  private Integer getNumber(String s) {
    log.debug("new input line: {}", s);
    // remove all letters
    String onlyNumber = s.replaceAll("[A-Za-z]", "");

    if (onlyNumber.length() == 0) {
      log.error("hum weird");
      return 0;
    }

    // keep first and last number
    String numberStr = String.valueOf(onlyNumber.charAt(0)) + String.valueOf(onlyNumber.charAt(onlyNumber.length() - 1));
    log.debug(numberStr);
    return Integer.valueOf(numberStr);
  }

  @SneakyThrows
  public void execute() {
    log.info("Day 1");

    List<String> lines = Util.fromFileLineToStringList("./day1/input");
    Integer val = lines.stream()
      .map(this::transformLetterToNumber)
      .map(this::getNumber)
      .reduce(Integer::sum)
      .orElse(0);

    log.info("Result: {}", val);
  }
}
