package org.nazstone.aoc.day10;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

@Getter
@Setter
@Builder
public class WallPosition {
    private Integer x;
    private Integer y;
    private String value;
    private Position internal;
    private Position external;
}
