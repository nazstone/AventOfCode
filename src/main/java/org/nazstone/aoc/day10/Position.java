package org.nazstone.aoc.day10;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@EqualsAndHashCode(exclude = "authorized")
public class Position {
    private int posX;
    private int posY;
    private List<String> authorized;
}
