package org.nazstone.aoc.day10;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NextPosition {
    private int posX;
    private int posY;
    private String type;
}
