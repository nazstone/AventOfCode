package org.nazstone.aoc.day10;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.*;

@Slf4j
public class Day10 {

    public static final String START_POINT = "S";
    public static final String PIPE = "|";
    public static final String UNDER = "-";
    public static final String J = "J";
    public static final String F = "F";
    public static final String L = "L";
    public static final String SEVEN = "7";
    private static final String DOT = ".";
    private static final String EMPTY = " ";

    @SneakyThrows
    public void execute() {
        log.info("Day 10");

        List<String> lines = Util.fromFileLineToStringList("./day10/input");
        String[][] arr = new String[lines.size()][lines.get(0).length()];
        int startX = -1;
        int startY = -1;
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            if (line.contains("S")) {
                startY = i;
                startX = line.indexOf(START_POINT);
            }

            arr[i] = line.split("");
        }

        log.debug("Start: ({},{}) {}", startX, startY, arr[startY][startX]);

        int lengthX = arr[0].length;
        int lengthY = arr.length;

        // navigate
        int posX = startX;
        int posY = startY;
        int prevX = startX;
        int prevY = startY;
        int i = 0;

        List<WallPosition>wallPosition = new ArrayList<>();


        while (true) {
            // increment count
            i += 1;

            // find next position to move
            // looking we
            List<Position> ps = getPossiblePositions(arr, posX, posY);

            Map<String, List<Position>> mapS = getPossiblePlaceS(posX, posY);

            // check the value
            List<Position> posTest = mapS.get(arr[posY][posX]);

            // found S ?
            if (posTest != null) {
                int finalPrevX = prevX;
                int finalPrevY = prevY;
                boolean s = posTest.stream()
                        .filter(p -> !(p.getPosX() == finalPrevX && p.getPosY() == finalPrevY))
                        .filter(p -> 0 <= p.getPosY() && p.getPosY() < lengthY
                                && 0 <= p.getPosX() && p.getPosX() < lengthX)
                        .anyMatch(p -> arr[p.getPosY()][p.getPosX()].equals(START_POINT));
                if (s) {
                    log.info("Found S at {}", i / 2);
                    break;
                }
            }

            // get next possibilty
            List<Position> psOk = new ArrayList<>();
            for (Position p : ps) {
                if (p.getPosY() < 0 || p.getPosY() >= lengthY) {
                    continue;
                }
                if (p.getPosX() < 0 || p.getPosX() >= lengthX) {
                    continue;
                }
                if (p.getAuthorized().contains(arr[p.getPosY()][p.getPosX()])) {
                    psOk.add(p);
                }
            }

            Position next;
            if (psOk.size() > 1) {
                int finalPrevX = prevX;
                int finalPrevY = prevY;
                next = psOk.stream()
                        .filter(p -> !(p.getPosX() == finalPrevX && p.getPosY() == finalPrevY))
                        .findAny()
                        .orElse(psOk.get(0));
            } else if (psOk.size() == 1){
                next = psOk.get(0);
            } else {
                log.error("pos: {},{}", posX, posY);
                throw new RuntimeException();
            }

            // backup old value
            prevX = posX;
            prevY = posY;

            // go to it
            wallPosition.add(WallPosition.builder().x(posX).y(posY).value(arr[posY][posX]).build());
            posY = next.getPosY();
            posX = next.getPosX();
        }

        // set wall
        String[][] arr2 = new String[arr.length][arr[0].length];
        for (int iy = 0; iy < arr2.length; iy += 1) {
            for (int ix = 0; ix < arr2[0].length; ix += 1) {
                arr2[iy][ix] = EMPTY;
            }
        }
        for (WallPosition wallPos : wallPosition) {
            arr2[wallPos.getY()][wallPos.getX()] = wallPos.getValue();
        }

        for (int y = 0; y < arr.length; y += 1) {
            for (int x = 0; x < arr[0].length; x += 1)
                if (arr[y][x].equals(DOT)) {
                    arr2[y][x] = DOT;
                }
        }

        List<String> arrSimpler = new ArrayList<>();
        for (String[] strings : arr2) {
            StringBuilder str = new StringBuilder();
            for (String s : strings) {
                str.append(s);
            }

            String newstr = str.toString();
            newstr = newstr.replaceAll("F[-]*J", "|");
            newstr = newstr.replaceAll("L[-]*7", "|");
            newstr = newstr.replaceAll("F[-]*7", "");
            newstr = newstr.replaceAll("L[-]*J", "");

            arrSimpler.add(newstr);
        }

        int count = 0;
        int allCount = 0;
        i = 0;
        for (String s : arrSimpler) {
            String[] lineSplit = s.split("");
            boolean inside = false;
            for (String chara : lineSplit) {
                if (chara.equals(PIPE)) {
                    inside = !inside;
                } else if (inside) {
                    count += 1;
                }
                if (chara.equals(DOT)) {
                    allCount += 1;
                }
            }
//            if (inside) {
//                log.error("Line {} in error: {}", i, s);
//                System.exit(1);
//            }
            log.debug(s);
            i += 1;
        }

        log.debug("count: {} - all: {}", count, allCount);
    }

    private static Map<String, List<Position>> getPossiblePlaceS(int posX, int posY) {
        Map<String, List<Position>> mapS = new HashMap<>();
        mapS.put(SEVEN, Arrays.asList(
                Position.builder().posX(posX - 1).posY(posY).build(),
                Position.builder().posX(posX).posY(posY + 1).build()));
        mapS.put(PIPE, Arrays.asList(
                Position.builder().posX(posX).posY(posY - 1).build(),
                Position.builder().posX(posX).posY(posY + 1).build()));
        mapS.put(F, Arrays.asList(
                Position.builder().posX(posX + 1).posY(posY).build(),
                Position.builder().posX(posX).posY(posY + 1).build()));
        mapS.put(L, Arrays.asList(
                Position.builder().posX(posX).posY(posY - 1).build(),
                Position.builder().posX(posX + 1).posY(posY).build()));
        mapS.put(J, Arrays.asList(
                Position.builder().posX(posX).posY(posY - 1).build(),
                Position.builder().posX(posX - 1).posY(posY).build()));
        mapS.put(UNDER, Arrays.asList(
                Position.builder().posX(posX - 1).posY(posY).build(),
                Position.builder().posX(posX + 1).posY(posY).build()));
        return mapS;
    }

    private static List<Position> getPossiblePositions(String[][] arr, int posX, int posY) {
        List<Position> ps = new ArrayList<>();
        switch (arr[posY][posX]) {
            case UNDER -> {
                ps.add(new Position(posX - 1, posY, Arrays.asList(F, L, UNDER)));
                ps.add(new Position(posX + 1, posY, Arrays.asList(J, SEVEN, UNDER)));
            }
            case PIPE -> {
                ps.add(new Position(posX, posY - 1, Arrays.asList(PIPE, SEVEN, F)));
                ps.add(new Position(posX, posY + 1, Arrays.asList(PIPE, L, J)));
            }
            case J -> {
                ps.add(new Position(posX, posY - 1, Arrays.asList(PIPE, SEVEN, F)));
                ps.add(new Position(posX - 1, posY, Arrays.asList(F, L, UNDER)));
            }
            case L -> {
                ps.add(new Position(posX, posY - 1, Arrays.asList(PIPE, SEVEN, F)));
                ps.add(new Position(posX + 1, posY, Arrays.asList(J, SEVEN, UNDER)));
            }
            case SEVEN -> {
                ps.add(new Position(posX, posY + 1, Arrays.asList(PIPE, L, J)));
                ps.add(new Position(posX - 1, posY, Arrays.asList(F, L, UNDER)));
            }
            case F -> {
                ps.add(new Position(posX + 1, posY, Arrays.asList(J, SEVEN, UNDER)));
                ps.add(new Position(posX, posY + 1, Arrays.asList(PIPE, L, J)));
            }
            default -> {
                // A
                ps.add(new Position(posX, posY - 1, Arrays.asList(PIPE, SEVEN, F)));
                // B
                ps.add(new Position(posX + 1, posY, Arrays.asList(J, SEVEN, UNDER)));
                // C
                ps.add(new Position(posX, posY + 1, Arrays.asList(PIPE, L, J)));
                // D
                ps.add(new Position(posX - 1, posY, Arrays.asList(F, L, UNDER)));
            }
        }
        return ps;
    }
}
