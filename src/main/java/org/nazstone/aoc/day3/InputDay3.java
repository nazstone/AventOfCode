package org.nazstone.aoc.day3;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@EqualsAndHashCode
public class InputDay3 {
    private Integer begin;
    private Integer end;
    private String value;
    private boolean symbol;
    private boolean hasSymbol;
}
