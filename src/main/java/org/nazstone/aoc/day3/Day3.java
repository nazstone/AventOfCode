package org.nazstone.aoc.day3;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class Day3 {
    @SneakyThrows
    public void execute() {
        List<String> lines = Util.fromFileLineToStringList("./day3/input");
        List<Result> resultsInput = lines.stream().map(s -> {
            List<InputDay3> inputs = new ArrayList<>();
            log.debug(s);
            Pattern pattern = Pattern.compile("\\d+", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(s);
            while (matcher.find()) {
                log.debug("number start index : {}-{} {}", matcher.start(), matcher.end(), matcher.group());
                boolean hasSymbol = (matcher.start() > 0 && !".".equals(s.substring(matcher.start() - 1, matcher.start())))
                        || (matcher.end() < s.length() && !".".equals(s.substring(matcher.end(), matcher.end() + 1)));
                inputs.add(
                        InputDay3.builder()
                                .begin(matcher.start())
                                .end(matcher.end())
                                .value(matcher.group())
                                .hasSymbol(hasSymbol)
                                .build());
            }
            pattern = Pattern.compile("[^\\d\\.]", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(s);
            while (matcher.find()) {
                log.debug("symbol start index symbol: {}-{} {}", matcher.start(), matcher.end(), matcher.group());
                inputs.add(
                        InputDay3.builder()
                                .begin(matcher.start())
                                .end(matcher.end())
                                .value(matcher.group())
                                .symbol(true)
                                .build());
            }
            return Result.builder().inputs(inputs).build();
        }).collect(Collectors.toList());

        Map.Entry<List<Integer>, List<Integer>> results = part1And2(resultsInput);
        log.info("result part 1: {}", results.getKey().stream().reduce(Integer::sum).orElse(0));

        log.info("result part 2: {}", results.getValue().stream().reduce(Integer::sum).orElse(0));
    }

    private Integer hasGear(InputDay3 wildcard, List<Result> resultsInput, int i) {
        int total = 0;
        if (!wildcard.isSymbol()) {
            return 0;
        }

        if (!"*".equals(wildcard.getValue())) {
            return 0;
        }

        Optional<InputDay3> numberBegin = Optional.empty();
        Optional<InputDay3> numberEnd = Optional.empty();

        for (int j = -1; j <= 1; j ++) {
            if (numberBegin.isEmpty()) {
                numberBegin = resultsInput.get(i + j).getInputs().stream()
                        .filter(s -> !s.isSymbol())
                        .filter(s -> s.getBegin() - 1 <= wildcard.getBegin() && wildcard.getBegin() <= s.getEnd())
                        .findAny();
            }
            if (numberEnd.isEmpty()) {
                Optional<InputDay3> finalNumberBegin = numberBegin;
                numberEnd = resultsInput.get(i + j).getInputs().stream()
                        .filter(s -> !s.isSymbol() && (finalNumberBegin.isEmpty() || !s.equals(finalNumberBegin.get())))
                        .filter(s -> s.getBegin() - 1 <= wildcard.getBegin() && wildcard.getBegin() <= s.getEnd())
                        .findAny();
            }
        }

        if (numberBegin.isPresent() && numberEnd.isPresent()) {
            log.debug("compute gear index: {} ({}, {})", i, numberBegin.get().getValue(), numberEnd.get().getValue());
            total += Integer.parseInt(numberBegin.get().getValue()) * Integer.parseInt(numberEnd.get().getValue());
        }

        return total;
    }// 87605697

    private AbstractMap.Entry<List<Integer>, List<Integer>> part1And2(List<Result> resultsInput) {
        List<Integer> results1 = new ArrayList<>();
        List<Integer> results2 = new ArrayList<>();

        for (int i = 0; i < resultsInput.size(); i++) {
            Result resTmp = resultsInput.get(i);
            for (InputDay3 input : resTmp.getInputs()) {
                // part1
                if (hasSymbol(input, resultsInput, i)) {
                    log.debug("Input {} hasSymbol", input.getValue());
                    results1.add(Integer.valueOf(input.getValue()));
                }
                // part 2
                Integer res = hasGear(input, resultsInput, i);
                results2.add(res);
            }
        }
        return new AbstractMap.SimpleEntry<>(results1, results2);
    } // 540212

    private boolean hasSymbol(InputDay3 input, List<Result> resultsInput, int i) {
        if (input.isSymbol()) {
            return false;
        }
        if (input.isHasSymbol()) {
            return true;
        }
        if (i > 0) {
            Result resTmp = resultsInput.get(i - 1);
            Optional<InputDay3> tada = resTmp.getInputs().stream()
                    .filter(InputDay3::isSymbol)
                    .filter(s -> input.getBegin() - 1  <= s.getBegin() && s.getEnd() <= input.getEnd() + 1)
                    .findAny();
            if (tada.isPresent()) {
                return true;
            }
        }

        if (i + 1 < resultsInput.size()) {
            Result resTmp = resultsInput.get(i + 1);
            Optional<InputDay3> tada = resTmp.getInputs().stream()
                    .filter(InputDay3::isSymbol)
                    .filter(s -> input.getBegin() - 1  <= s.getBegin() && s.getEnd() <= input.getEnd() + 1)
                    .findAny();
            if (tada.isPresent()) {
                return true;
            }
        }
        return false;
    }
}
