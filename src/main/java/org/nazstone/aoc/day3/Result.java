package org.nazstone.aoc.day3;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Setter
@Getter
public class Result {
    private List<InputDay3> inputs;
}
