package org.nazstone.aoc.day14;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.nazstone.aoc.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Day14 {
    public static final String ROUND_ROCK = "O";
    public static final int cycle = 1_000_000_000;
    public static final String DELIMITER = " ";

    @SneakyThrows
    public void execute() {
        log.info("Day 14");

        List<String> lines = Util.fromFileLineToStringList("./day14/input");
        String[][] grid = new String[lines.get(0).length()][lines.size()];
        convertToGrid(lines, grid);

        List<Integer> previousComputeVal = new ArrayList<>();
        Pair<Integer, List<Integer>> pair = null;

        for (int i = 0; i < cycle; i += 1) {
            compute(grid); // north
            grid = reverse(grid);

            compute(grid); // west
            grid = reverse(grid);

            compute(grid); // south
            grid = reverse(grid);

            compute(grid); // east
            grid = reverse(grid);

            int compute = calculateGrid(grid);

            previousComputeVal.add(compute);

            pair = hasCycle(previousComputeVal);
            if (pair != null) {
                break;
            }
        }
        if (pair != null) {
            int idx = (cycle - pair.getValue0() - 1) % pair.getValue1().size();
            log.debug("Compute value: {}", pair.getValue1().get(idx));
        }
    }

    private Pair<Integer, List<Integer>> hasCycle(List<Integer> previousComputeVal) {
        String allCalculate = previousComputeVal.stream().map(String::valueOf).collect(Collectors.joining(DELIMITER));

        for (int j = 0; j < previousComputeVal.size(); j += 1) {
            List<Integer> sb = new ArrayList<>();
            for (int i = j; i < previousComputeVal.size(); i += 1) {
                sb.add(previousComputeVal.get(i));

                if (sb.size() > 2) {
                    String freq = sb.stream().map(String::valueOf).collect(Collectors.joining(DELIMITER));
                    if (countCycleInCalculateStr(allCalculate, freq)) {
                        return Pair.with(j, sb);
                    }
                }
            }
        }

        return null;
    }

    private boolean countCycleInCalculateStr(String allCalculate, String freq) {
        String copyAllCalculate = allCalculate;
        while (true) {
            int indexFreq = copyAllCalculate.indexOf(freq);
            if (indexFreq >= 0) {
                copyAllCalculate = copyAllCalculate.substring(indexFreq + freq.length());
                if (copyAllCalculate.indexOf(freq) == DELIMITER.length()) {
                   return true;
                }
            } else {
                break;
            }
        }
        return false;
    }


    private String[][] reverse(String[][] matrix) {
        int size = matrix.length;
        String[][] ret = new String[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                ret[size - 1 - i][j] = matrix[j][i]; //***
            }
        }
        return ret;
    }

    private void convertToGrid(List<String> lines, String[][] grid) {
        for (int y = 0; y < lines.size(); y++) {
            String line = lines.get(y);
            for (int x = 0; x < line.length(); x += 1) {
                grid[x][y] = String.valueOf(line.charAt(x));
            }
        }
    }

    private void compute(String[][] grid) {
        for (int x = 0; x < grid.length; x += 1) {
            for (int y = 1; y < grid[x].length; y += 1) {
                if (grid[x][y].equals(ROUND_ROCK)) {
                    for (int y2 = y - 1; y2 >= 0; y2 -= 1) {
                        if (grid[x][y2].equals(ROUND_ROCK) || grid[x][y2].equals("#")) {
                            grid[x][y] = ".";
                            grid[x][y2 + 1] = ROUND_ROCK;
                            break;
                        } else if (y2 == 0) {
                            grid[x][y] = ".";
                            grid[x][y2] = ROUND_ROCK;
                        }
                    }
                }
            }
        }
    }

    private Integer calculateGrid(String[][] grid) {
        int total = 0;
        for (int y = 0; y < grid[0].length; y += 1) {
            for (String[] strings : grid) {
                if (strings[y].equals(ROUND_ROCK)) {
                    total += grid[0].length - y;
                }
            }
        }
        return total;
    }
}