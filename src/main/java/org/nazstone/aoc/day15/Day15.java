package org.nazstone.aoc.day15;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.*;

@Slf4j
public class Day15 {
    @SneakyThrows
    public void execute() {
        log.info("Day 15");

        List<String> list = Util.fromFileOneLineCommaToStringList("day15/input");

        int val = sumHash(list);
        log.info("Part 1 sum hash: {}", val);

        Map<Integer, List<Lens>> map = fillBox(list);
        Integer res = calculateForBox(map);
        log.info("Part 2 sum box: {}", res);
    }

    public Integer calculateForBox(Map<Integer, List<Lens>> map) {
        int count = 0;
        for (Map.Entry<Integer, List<Lens>> integerMapEntry : map.entrySet()) {
            count += integerMapEntry.getValue().stream().map(e -> (integerMapEntry.getKey() + 1) * e.getSort() * e.getLength()).reduce(Integer::sum).orElse(0);
        }
        return count;
    }

    public Map<Integer, List<Lens>> fillBox(List<String> list) {
        Map<Integer, List<Lens>> map = new HashMap<>();

        for (String tmp : list) {
            List<Lens> lenss = null;
            if (tmp.contains("=")) {
                String[] labelLength = tmp.split("=");
                // get map
                lenss = map.computeIfAbsent(hash(labelLength[0]), k -> new ArrayList<>());
                // get sort
                Integer sort;
                Optional<Lens> lensTmp = lenss.stream().filter(e -> e.getLabel().equals(labelLength[0])).findAny();
                if (lensTmp.isPresent()) {
                    lensTmp.get().setLength(Integer.valueOf(labelLength[1]));
                } else {
                    sort = lenss.stream().map(Lens::getSort).map( e -> e + 1).max(Integer::compare).orElse(0);
                    // insert lens
                    Lens lens = Lens.builder()
                            .label(labelLength[0])
                            .length(Integer.valueOf(labelLength[1]))
                            .sort(sort)
                            .build();
                    lenss.add(lens);
                }
            } else if (tmp.contains("-")) {
                String[] labelLength = tmp.split("-");
                // get map
                lenss = map.get(hash(labelLength[0]));
                if (lenss == null) {
                    continue;
                }
                // found to remove
                Optional<Lens> lensToRemove = lenss.stream().filter(e -> e.getLabel().equals(labelLength[0])).findFirst();
                // remove
                lensToRemove.ifPresent(lenss::remove);
            }
            if (lenss != null) {
                // reorder sort
                lenss.sort((a, b) -> a.getSort() - b.getSort());
                for (int i = 0; i < lenss.size(); i++) {
                    lenss.get(i).setSort(i + 1);
                }
            }
        }
        return map;
    }

    public int sumHash(List<String> list) {
        return list.stream()
                .map(this::hash)
                .reduce(Integer::sum)
                .orElse(0);
    }

    public int hash(String input) {
        return input.chars()
                .reduce(0, (a, c) -> ((a + c) * 17) % 256);
    }
}
