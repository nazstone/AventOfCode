package org.nazstone.aoc.day15;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class Lens {
  private String label;
  private Integer length;
  private Integer sort;
}
