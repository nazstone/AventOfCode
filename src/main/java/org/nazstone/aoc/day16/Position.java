package org.nazstone.aoc.day16;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Position {
    private int x;
    private int y;
}
