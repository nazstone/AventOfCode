package org.nazstone.aoc.day16;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.nazstone.aoc.util.Util;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Day16 {
    // part 2 11298 too high
    @Setter
    @Getter
    private String[][] grid;
    @Setter
    private String[][] gridTile;
    private final List<String> historyPath = new ArrayList<>();

    @SneakyThrows
    public void execute() {
        log.info("Day 16");

        grid = Util.fromFileToGrid("day16/input");
        gridTile = new String[grid.length][grid[0].length];

        int count = part1();
        log.info("Count: {}", count);

        count = part2();
        log.info("Count2: {}", count);

    }

    public int part1() {
        Position actualPosition = Position.builder().x(0).y(0).build();
        Position direction = Position.builder().x(1).y(0).build();

        letsgo(actualPosition, direction);
        return countTile();
    }

    public int part2() {
        int max = 0;

        List<Position> directions = new ArrayList<>();
        directions.add(Position.builder().x(1).y(0).build());
        directions.add(Position.builder().x(-1).y(0).build());
        directions.add(Position.builder().x(0).y(1).build());
        directions.add(Position.builder().x(0).y(-1).build());

        List<Pair<Position, Position>> pairs = new ArrayList<>();
        for (int x = 0; x < grid.length; x += 1) {
            for (Position direction : directions) {
                pairs.add(Pair.with(Position.builder().x(x).y(0).build(), direction));
                pairs.add(Pair.with(Position.builder().x(x).y(grid.length - 1).build(), direction));
            }
        }
        for (int y = 0; y < grid[0].length; y += 1) {
            for (Position direction : directions) {
                pairs.add(Pair.with(Position.builder().x(0).y(y).build(), direction));
                pairs.add(Pair.with(Position.builder().x(grid[0].length - 1).y(y).build(), direction));
            }
        }

        for (Pair<Position, Position> pair : pairs) {
            reset();
            letsgo(pair.getValue0(), pair.getValue1());
            int countTile = countTile();
            max = Math.max(countTile, max);
        }

        return max;
    }

    private void reset() {
        historyPath.clear();
        setGridTile(new String[grid.length][grid[0].length]);
    }

    public void letsgo(Position actualPosition, Position direction) {
        Quartet<Position, Position, Position, Position> quartlet = calculateNewPosition(actualPosition, direction);

        if (nothingInHistory(quartlet.getValue0(), quartlet.getValue1())) {
            letsgo(quartlet.getValue0(), quartlet.getValue1());
        }

        if (quartlet.getValue2() != null && quartlet.getValue3() != null && nothingInHistory(quartlet.getValue2(), quartlet.getValue3())) {
            letsgo(quartlet.getValue2(), quartlet.getValue3());
        }
    }

    private boolean nothingInHistory(Position position, Position direction) {
        if (position == null) {
            return false;
        }
        String key = String.format("(%d,%d)-(%d,%d)", position.getX(), position.getY(), direction.getX(), direction.getY());
        if (historyPath.contains(key)) {
            return false;
        }
        historyPath.add(key);
        return true;
    }

    public Quartet<Position, Position, Position, Position> calculateNewPosition(Position actualPosition, Position direction) {
        Position pos1 = null;
        Position pos2 = null;
        Position newDirection1 = null;
        Position newDirection2 = null;

        switch (grid[actualPosition.getX()][actualPosition.getY()]) {
            case ".":
                newDirection1 = Position.builder()
                        .x(direction.getX())
                        .y(direction.getY())
                        .build();
                pos1 = Position.builder()
                        .x(actualPosition.getX() + direction.getX())
                        .y(actualPosition.getY() + direction.getY())
                        .build();

                if (positionIsOutsideGrid(pos1)) {
                    pos1 = null;
                }
                break;
            case "\\":
                newDirection1 = Position.builder()
                        .x(direction.getY())
                        .y(direction.getX())
                        .build();
                pos1 = Position.builder()
                        .x(actualPosition.getX() + newDirection1.getX())
                        .y(actualPosition.getY() + newDirection1.getY())
                        .build();

                if (positionIsOutsideGrid(pos1)) {
                    pos1 = null;
                }
                break;
            case "/":
                newDirection1 = Position.builder()
                        .x(direction.getY() * -1)
                        .y(direction.getX() * -1)
                        .build();
                pos1 = Position.builder()
                        .x(actualPosition.getX() + newDirection1.getX())
                        .y(actualPosition.getY() + newDirection1.getY())
                        .build();
                if (positionIsOutsideGrid(pos1)) {
                    pos1 = null;
                }
                break;
            case "|":
                // manage up
                newDirection1 = Position.builder()
                        .x(0)
                        .y(1)
                        .build();
                pos1 = Position.builder()
                        .x(actualPosition.getX() + newDirection1.getX())
                        .y(actualPosition.getY() + newDirection1.getY())
                        .build();
                if (positionIsOutsideGrid(pos1)) {
                    pos1 = null;
                }
                // manage down
                newDirection2 = Position.builder()
                        .x(0)
                        .y(-1)
                        .build();
                pos2 = Position.builder()
                        .x(actualPosition.getX() + newDirection2.getX())
                        .y(actualPosition.getY() + newDirection2.getY())
                        .build();
                if (positionIsOutsideGrid(pos2)) {
                    pos2 = null;
                }
                break;

            case "-":
                // manage left
                newDirection1 = Position.builder()
                        .x(-1)
                        .y(0)
                        .build();
                pos1 = Position.builder()
                        .x(actualPosition.getX() + newDirection1.getX())
                        .y(actualPosition.getY() + newDirection1.getY())
                        .build();
                if (positionIsOutsideGrid(pos1)) {
                    pos1 = null;
                }
                // manage right
                newDirection2 = Position.builder()
                        .x(1)
                        .y(0)
                        .build();
                pos2 = Position.builder()
                        .x(actualPosition.getX() + newDirection2.getX())
                        .y(actualPosition.getY() + newDirection2.getY())
                        .build();
                if (positionIsOutsideGrid(pos2)) {
                    pos2 = null;
                }
                break;
        }
        if ("#".equals(gridTile[actualPosition.getX()][actualPosition.getY()])) {
            gridTile[actualPosition.getX()][actualPosition.getY()] = "2";
        } else {
            gridTile[actualPosition.getX()][actualPosition.getY()] = "#";
        }
        return Quartet.with(pos1, newDirection1, pos2, newDirection2);
    }

    private boolean positionIsOutsideGrid(Position pos) {
        if (pos.getX() < 0 || pos.getX() >= grid.length) {
            return true;
        }
        return pos.getY() < 0 || pos.getY() >= grid[0].length;
    }

    public Integer countTile() {
        int count = 0;
        for (String[] strings : gridTile) {
            for (int y = 0; y < gridTile[0].length; y++) {
                if ("#".equals(strings[y]) || "2".equals(strings[y])) {
                    count += 1;
                }
            }
        }
        return count;
    }

    public void logGrid() {
        for (int y = 0; y < gridTile[0].length; y++) {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < gridTile[0].length; x++) {
                if (this.gridTile[x][y] != null) {
                    sb.append(this.gridTile[x][y]);
                } else {
                    sb.append(this.grid[x][y]);
                }
            }
            log.debug(sb.toString());
        }
    }
}