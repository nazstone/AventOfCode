package org.nazstone.aoc.day11;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.nazstone.aoc.util.Util;

import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;

@Slf4j
public class Day11 {


    public static final String GALAXY = "#";
    public static final String HUGE_DOT = "*";
    private static final long SIZE_EMPTY = 999_999;


    @SneakyThrows
    public void execute() {
        log.info("Day 11");

        List<String> lines = Util.fromFileLineToStringList("./day11/input");

        Pair<List<Integer>, List<Integer>> pairEmpties = getEmpties(lines);

        List<Integer> emptyRows = pairEmpties.getValue0();
        List<Integer> emptyCols = pairEmpties.getValue1();

        log.debug("empty row: {}", emptyRows.size());
        log.debug("empty cols: {}", emptyCols.size());

        lines = newLineRows(lines, emptyRows);
        lines = newLineCols(lines, emptyCols);

        List<Position> galaxies = getGalaxies(lines);

        Set<Pair<Position, Position>> galaxiesCouple = getPairsGalaxies(galaxies);

        lines.forEach(log::debug);

        log.debug("Number of galaxies: {}", galaxies.size());
        log.debug("Number of couple galaxies: {}", galaxiesCouple.size());

        BigInteger count = galaxiesCouple.stream()
                .map(getPairIntegerFunction())
                .map(e -> new BigInteger(String.valueOf(e)))
                .reduce(BigInteger::add)
                .orElse(BigInteger.valueOf(0L));
        log.info("Galaxie distance count: {}", count);
    }

    private static Set<Pair<Position, Position>> getPairsGalaxies(List<Position> galaxies) {
        Set<Pair<Position, Position>> galaxiesCouple = new HashSet<>();
        for (int i = 0; i < galaxies.size(); i += 1) {
            for (int j = i; j < galaxies.size(); j += 1) {
                if (i == j) {
                    continue;
                }
                galaxiesCouple.add(Pair.with(galaxies.get(i), galaxies.get(j)));
            }
        }
        return galaxiesCouple;
    }


    private Pair<List<Integer>, List<Integer>> getEmpties(List<String> lines) {
        List<Integer> emptyRows = new ArrayList<>();
        List<Integer> emptyCols = new ArrayList<>();
        String[][] arr = new String[lines.get(0).length()][lines.size()];

        // found empty rows cols
        for (int iy = 0; iy < lines.size(); iy++) {
            String line = lines.get(iy);

            if (!line.contains(GALAXY)) {
                emptyRows.add(iy);
            }

            String[] cols = line.split("");
            for (int ix = 0; ix < cols.length; ix++) {
                arr[ix][iy] = cols[ix];
            }
        }
        for (int ix = 0; ix < arr.length; ix++) {
            boolean empty = true;
            for (int iy = 0; iy < arr[0].length; iy++) {
                if (arr[ix][iy].equals(GALAXY)) {
                    empty = false;
                    break;
                }
            }
            if (empty) {
                emptyCols.add(ix);
            }
        }
        return Pair.with(emptyRows, emptyCols);
    }

    private List<Position> getGalaxies(List<String> lines) {
        List<Position> galaxies = new ArrayList<>();

        for (int iy = 0; iy < lines.size(); iy++) {
            String line = lines.get(iy);
            String[] cols = line.split("");
            for (int ix = 0; ix < cols.length; ix++) {
                if (cols[ix].equals(GALAXY)) {
                    long countX = countXHugeEmpty(lines, ix, iy);
                    long countY = countYHugeEmpty(lines, ix, iy);

                    galaxies.add(Position.builder()
                            .x((int) countX)
                            .y((int) countY)
                            .value(cols[ix])
                            .build());

                    log.debug("({}, {}) => ({}, {})", ix, iy, countX, countY);
                }
            }
        }
        return galaxies;
    }

    private long countXHugeEmpty(List<String> lines, int ix, int iy) {
        long count = 0;
        String line = lines.get(iy);
        for (int i = 0; i < line.length() && i < ix; i += 1) {
            if (line.startsWith(HUGE_DOT, i)) {
                count += SIZE_EMPTY;
            } else {
                count += 1;
            }
        }
        return count;
    }

    private long countYHugeEmpty(List<String> lines, int ix, int iy) {
        long count = 0;
        for (int i = 0; i < lines.size() && i < iy; i++) {
            String line = lines.get(i);
            String val = line.substring(ix, ix + 1);
            if (val.equals(HUGE_DOT)) {
                count += SIZE_EMPTY;
            } else {
                count += 1;
            }
        }
        return count;
    }

    private List<String> newLineCols(List<String> lines, List<Integer> emptyCols) {
        List<String> linesTmp = new ArrayList<>();
        for (String line : lines) {
            int insertedCol = 0;
            for (Integer emptyCol : emptyCols) {
                line = new StringBuilder(line).insert(emptyCol + insertedCol, HUGE_DOT).toString();
                insertedCol += 1;
            }
            linesTmp.add(line);
        }
        return linesTmp;
    }

    private List<String> newLineRows(List<String> lines, List<Integer> emptyRows) {
        List<String> linesTmp = new ArrayList<>(lines);
        int insertedLine = 0;

        for (Integer emptyRow : emptyRows) {
            String newLine = "";
            for (int i = 0; i < lines.get(0).length(); i += 1){
                newLine = newLine.concat(HUGE_DOT);
            }
            linesTmp.add(emptyRow + insertedLine, newLine);
            insertedLine += 1;
        }
        return linesTmp;
    }

    private static Function<Pair<Position, Position>, Integer> getPairIntegerFunction() {
        return e -> Math.abs(e.getValue0().getX() - e.getValue1().getX()) + Math.abs(e.getValue0().getY() - e.getValue1().getY());
    }
}
