package org.nazstone.aoc.day11;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@EqualsAndHashCode
public class Position {
    private Integer x;
    private Integer y;
    private String value;
}
