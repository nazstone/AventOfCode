package org.nazstone.aoc.day13;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Day13 {
    // Too low
    // 18391
    // 21927
    // good: 33728
    // Too high
    // 37692
    @SneakyThrows
    public void execute() {
        log.info("Day 13");

        List<String> lines = Util.fromFileLineToStringList("./day13/input");

        List<List<String>> texts = transformToInput(lines);
        int count = 0;
        int index = 0;

        for (List<String> text : texts) {
            log.debug("index: {}", index ++);
            List<String> textReverse = reverse(text);

            // try vertical
            int mirrorLineV = mirrorLine(textReverse);
            if (log.isDebugEnabled()) {
                log.debug("Vertical: {}", mirrorLineV);
            }

            int mirrorLineH = mirrorLine(text);
            if (log.isDebugEnabled()) {
                log.debug("Horizontal: {}", mirrorLineH);
            }

            log.debug("\n");
            if (mirrorLineV > 0) {
                count += mirrorLineV;
            } else {
                count += mirrorLineH * 100;
            }
        }

        log.info("Count: {}", count);
    }

    private List<String> reverse(List<String> text) {
        List<String> reverseText = new ArrayList<>();

        int textLength = text.get(0).length();
        for (int j = 0; j < textLength; j += 1) {
            StringBuilder newStr = new StringBuilder();
            for (String str : text) {
                newStr.append(str.charAt(j));
            }
            reverseText.add(newStr.toString());
        }
        return reverseText;
    }

    private int mirrorLine(List<String> text) {
        int max = 0;

        boolean found = false;
        int lastLine = text.size() - 1;
        for (int i = 0; i <= lastLine; i++) {
            String line = text.get(i);

            // search for line if mirror ok
            List<Integer> indexesFound = new ArrayList<>();
            for (int i1 = i + 1; i1 < text.size(); i1++) {
                if (text.get(i1).contains(line)) {
                    indexesFound.add(i1);
                }
            }

            for (Integer indexFound : indexesFound) {
                int lastIndexOf = indexFound;
                found = false;
                if (lastIndexOf == lastLine || i == 0 && lastIndexOf > 0) {
                    for (int j = 1; j <= (lastIndexOf - i) / 2; j += 1) {
                        if (!text.get(j + i).equals(text.get(lastIndexOf - j))) {
                            found = false;
                            break;
                        } else {
                            found = true;
                        }
                    }
                    if (lastIndexOf - i == 1) {
                        return i + 1;
                    }
                    if (found) {
                        return i + (lastIndexOf - i) / 2 + 1;
                    }
                }
            }
        }

        return max;
    }

    private List<List<String>> transformToInput(List<String> lines) {
        List<List<String> > all = new ArrayList<>();
        List<String> tmp = new ArrayList<>();
        for (String line : lines) {
            if (line.length() == 0) {
                all.add(tmp);
                tmp = new ArrayList<>();
                continue;
            }
            tmp.add(line);
        }
        all.add(tmp);
        return all;
    }

}
