package org.nazstone.aoc.day18;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.*;

@Slf4j
public class Day18 {

    Map<String, Position> mapDirection = Map.of(
        "R", new Position(1, 0),
        "L", new Position(-1, 0),
        "D", new Position(0, 1),
        "U", new Position(0, -1)
    );
    Map<String, String> mapDirection2 = Map.of(
            "0", "R",
            "1", "D",
            "2", "L",
            "3", "U"
    );

    @SneakyThrows
    public void execute() {
        log.info("Day 18");

        List<Input> inputs = getInputs2();

        long area = getDimEtc(inputs);

        log.debug("Part1: {}", area);
    }

    public List<Input> getInputs() {
        List<String> lines = Util.fromFileLineToStringList("day18/input");
        List<Input> inputs = new ArrayList<>();
        for (String line : lines) {
            String[] lineSplit = line.split(" ");
            Input input = Input.builder()
                    .direction(lineSplit[0])
                    .length(Double.parseDouble(lineSplit[1]))
                    .rest(lineSplit[2])
                    .build();

            inputs.add(input);
        }
        return inputs;
    }

    public List<Input> getInputs2() {
        List<String> lines = Util.fromFileLineToStringList("day18/input");
        List<Input> inputs = new ArrayList<>();
        for (String line : lines) {
            String[] lineSplit = line.split(" ");
            String sub = lineSplit[2].substring(2, 8);
            int val = Integer.parseInt(sub.substring(0, 5), 16);
            Input input = Input.builder()
                    .direction(mapDirection2.get(sub.substring(5)))
                    .length((double) val)
                    .rest(lineSplit[2])
                    .build();

            inputs.add(input);
        }
        return inputs;
    }

    public long getDimEtc(List<Input> inputs) {
        List<Position> summits = new ArrayList<>();
        summits.add(new Position(0,0));

        long countX = 0l;
        long countY = 0l;

        for (Input input : inputs) {
            if (input.getDirection().equals("L")) {
                countX -= input.getLength();
            } else if (input.getDirection().equals("R")) {
                countX += input.getLength();
            } else if (input.getDirection().equals("D")) {
                countY += input.getLength();
            } else if (input.getDirection().equals("U")) {
                countY -= input.getLength();
            }
            summits.add(new Position((int) countX, (int) countY));
        }

        for (Position summit : summits) {
            summit.setX(summit.getX());
            summit.setY(summit.getY());
        }

        return calculateArea(summits);
    }

    public long calculateArea(List<Position> summits) {
        long count = 0;
        long perimetre = 0;
        for (int i = 0; i < summits.size(); i += 1) {
            long x1 = summits.get(i).getX();
            long y1 = summits.get(i).getY();
            long x2 = summits.get((i + 1) % summits.size()).getX();
            long y2 = summits.get((i + 1) % summits.size()).getY();
            count += (x1 * y2 - x2 * y1);
            if (count > Long.MAX_VALUE) {
                log.error("tada");
            }
            perimetre += Math.abs(x1 - x2 + y1 - y2);
        }
        return perimetre / 2 + count / 2 + 1;
    }
}