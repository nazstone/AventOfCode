package org.nazstone.aoc.day18;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Input {
  private String direction;
  private Double length;
  private String rest;
}
