package org.nazstone.aoc;

import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.day19.Day19;

@Slf4j
public class Main {
  public static void main(String[] args) {

    try {
      long begin = System.currentTimeMillis();
      new Day19().execute();
      log.info("Time measured: {}", System.currentTimeMillis() - begin);
    } catch (Exception e) {
      log.error("I don't like this", e);
    }
  }
}