package org.nazstone.aoc.day7;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CardResult {
    private String value;
    private Count count;
    private int bid;
    private int rank;
    private int highest;
}
