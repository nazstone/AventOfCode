package org.nazstone.aoc.day7;

import lombok.Getter;

@Getter
public enum Type {
    FIVE_KIND(1),
    FOUR_KIND(2),
    FULL_HOUSE(3),
    THREE_KIND(4),
    TWO_PAIR(5),
    ONE_PAIR(6),
    HIGH_CARD(7);
    private final int order;

    Type(int i) {
        this.order = i;
    }



}
