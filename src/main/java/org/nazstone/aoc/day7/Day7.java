package org.nazstone.aoc.day7;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.util.*;

@Slf4j
public class Day7 {
    private final List<Character> orderedChar = Arrays.stream("J 2 3 4 5 6 7 8 9 T Q K A".split("\\s+")).map(s -> s.charAt(0)).toList();
    @SneakyThrows
    public void execute() {
        log.info("Day 7");

        List<String> lines = Util.fromFileLineToStringList("./day7/input");
        List<CardResult> handList = lines.stream().map(s -> {
            String[] tmp = s.split("\\s+");
            return CardResult.builder()
                    .bid(Integer.parseInt(tmp[1]))
                    .count(kind2(tmp[0]))
                    .value(tmp[0])
                    .build();
        }).toList();
        List<CardResult> resultOrdered = handList.stream()
                .sorted((e1, e2) -> {
                    if (e1.getCount().getType().equals(e2.getCount().getType())) {
                        return compareForType(e1.getCount().getType(), e1, e2);
                    }
                    return e2.getCount().getType().getOrder() - e1.getCount().getType().getOrder();
                })
                //.sorted(Comparator.comparingDouble(s -> s.getCount().getMyPart()))
                .toList();

        int res = 0;
        for (int i = 0; i < resultOrdered.size(); i += 1) {
            CardResult cardResult = resultOrdered.get(i);
            res += (cardResult.getBid() * (i + 1));
            log.debug("value: {} - bid * rank: {} * {}, type: {} - string4debug: {} - sum: {}",
                    cardResult.getValue(), cardResult.getBid(), (i + 1), cardResult.getCount().getType().toString(), cardResult.getCount().getDebug(), res);
        }

        log.info("Result : {}", res);
    }

    private int compareForType(Type type, CardResult e1, CardResult e2) {
        for (int i = 0; i < e1.getValue().length(); i += 1) {
            if (e1.getValue().charAt(i) != e2.getValue().charAt(i)) {
                return getOrderedChar(e1.getValue().charAt(i)) - getOrderedChar(e2.getValue().charAt(i));
            }
        }
        return 0;
    }


    private Count kind(String s) {
        long max = 0;
        Map<Character, Long> mapMax = new HashMap<>();
        for (int i = 0; i < s.length(); i += 1) {
            int finalI = i;
            long firstChara = s.chars().filter(num -> num == s.charAt(finalI)).count();
            mapMax.put(s.charAt(finalI), Math.max(mapMax.getOrDefault(s.charAt(finalI), 0L), firstChara));
            max = Math.max(max, firstChara);
        }

        if (max == 5) {
            return Count.builder()
                    .type(Type.FIVE_KIND)
                    .debug(s)
                    .build();
        } else if (max == 4) {
            return Count.builder()
                    .type(Type.FOUR_KIND)
                    .build();
        }

        boolean threeCard = mapMax.entrySet().stream().anyMatch(x -> x.getValue() == 3);
        if (threeCard) {
            boolean twoCard = mapMax.entrySet().stream().anyMatch(x -> x.getValue() == 2);
            if (twoCard) {
                return Count.builder()
                        .type(Type.FULL_HOUSE)
                        .build();
            } else {
                return Count.builder()
                        .type(Type.THREE_KIND)
                        .build();
            }
        }

        boolean twoPair = mapMax.entrySet().stream().filter(x -> x.getValue() != 1).count() == 2;
        if (twoPair) {
            return Count.builder()
                    .type(Type.TWO_PAIR)
                    .build();
        }

        boolean onePair = mapMax.entrySet().stream().anyMatch(x -> x.getValue() == 2);
        if (onePair) {
            return Count.builder()
                    .type(Type.ONE_PAIR)
                    .build();
        }

        return Count.builder()
                .type(Type.HIGH_CARD)
                .build();
    }

    private Count kind2(String s) {
        Map<Character, Integer> mapMax = getMax(s);

        if (s.contains("J")) {
            Character chara = mapMax.entrySet().stream()
                    .filter(e -> e.getKey() != 'J')
                    .max(Comparator.comparingInt(Map.Entry::getValue))
                    .map(Map.Entry::getKey)
                    .orElse('A');

            s = s.replaceAll("J", String.valueOf(chara));
        }

        mapMax = getMax(s);
        long max = mapMax.values().stream().reduce(Math::max).get();

        if (max == 5) {
            return Count.builder()
                    .type(Type.FIVE_KIND)
                    .build();
        } else if (max == 4) {
            return Count.builder()
                    .type(Type.FOUR_KIND)
                    .build();
        }

        boolean threeCard = mapMax.entrySet().stream().anyMatch(x -> x.getValue() == 3);
        if (threeCard) {
            boolean twoCard = mapMax.entrySet().stream().anyMatch(x -> x.getValue() == 2);
            if (twoCard) {
                return Count.builder()
                        .type(Type.FULL_HOUSE)
                        .build();
            } else {
                return Count.builder()
                        .type(Type.THREE_KIND)
                        .build();
            }
        }

        boolean twoPair = mapMax.entrySet().stream().filter(x -> x.getValue() != 1).count() == 2;
        if (twoPair) {
            return Count.builder()
                    .type(Type.TWO_PAIR)
                    .build();
        }

        boolean onePair = mapMax.entrySet().stream().anyMatch(x -> x.getValue() == 2);
        if (onePair) {
            return Count.builder()
                    .type(Type.ONE_PAIR)
                    .build();
        }

        return Count.builder()
                .type(Type.HIGH_CARD)
                .build();
    }

    private static Map<Character, Integer> getMax(String s) {
        Map<Character, Integer> mapMax = new HashMap<>();
        for (int i = 0; i < s.length(); i += 1) {
            int finalI = i;
            int firstChara = (int) s.chars().filter(num -> num == s.charAt(finalI)).count();
            mapMax.put(s.charAt(finalI), Math.max(mapMax.getOrDefault(s.charAt(finalI), 0), firstChara));
            // improve if count == 5
        }
        return mapMax;
    }

    private int getOrderedChar(char c) {
        return orderedChar.indexOf(c) + 1;
    }
}
