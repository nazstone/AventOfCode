package org.nazstone.aoc.day7;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Count {
    private Type type;
    private int myPart;
    private String debug;
}
