package org.nazstone.aoc.day8;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Coord {
    private String left;
    private String right;
}
