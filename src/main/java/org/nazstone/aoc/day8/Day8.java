package org.nazstone.aoc.day8;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.nazstone.aoc.util.Util;

import java.math.BigInteger;
import java.util.*;

@Slf4j
public class Day8 {
    @SneakyThrows
    public void execute() {
        log.info("Day 8");

        List<String> lines = Util.fromFileLineToStringList("./day8/input");
        List<String> instruction = Arrays.stream(lines.get(0).split("")).toList();

        Map<String, Coord> map = new HashMap<>();

        for (int i = 2; i < lines.size(); i += 1) {
            String line = lines.get(i);
            String[] split = line.split("=");
            String key = split[0].trim();
            String[] next = split[1].replaceAll("[()]", "").trim().split(",");
            String left = next[0].trim();
            String right = next[1].trim();

            map.put(key, Coord.builder()
                    .right(right)
                    .left(left)
                    .build());
        }

        List<String> nextKeys = new ArrayList<>();
        for (String key : map.keySet()) {
            if (key.endsWith("A")) {
                nextKeys.add(key);
            }
        }

        log.debug("nextKey : {}", nextKeys);
        BigInteger lcmVal = new BigInteger("1");
        for (String nextKey : nextKeys) {
            int i = findZZZ(instruction, map, nextKey);
            BigInteger iVal = new BigInteger(String.valueOf(i));
            lcmVal = lcmVal.multiply(iVal).divide(lcmVal.gcd(iVal));
            log.debug("input: {} => {}", nextKey, i);
        }
        log.info("res: {}", lcmVal);
    }

    private Integer findZZZ(List<String> instruction, Map<String, Coord> map, String nextKey) {
        String nextKeyOrig = nextKey;
        int i = 0;
        while (true) {
            String instr = instruction.get(i % instruction.size());
            Coord coord = map.get(nextKey);
            if ("R".equals(instr)) {
                nextKey = coord.getRight();
            } else {
                nextKey = coord.getLeft();
            }

            if (nextKey.endsWith("Z")) {
                log.debug("for {} found {} after {}", nextKeyOrig, nextKey, i);
                return i + 1;
            }

            i += 1;
        }
    }
}
