package org.nazstone.aoc.day9;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.nazstone.aoc.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
public class Day9 {
    @SneakyThrows
    public void execute() {
        log.info("Day 9");

        List<String> lines = Util.fromFileLineToStringList("./day9/input1");

        List<Pair<Integer, Integer>> phase1 = lines.stream()
                .map(s -> Arrays.stream(s.split("\\s+")).map(Integer::parseInt).toList())
                .map(Day9::getNextVal)
                .toList();
        Integer resultMin = phase1.stream()
                .map(Pair::getValue0)
                .reduce(Integer::sum)
                .orElse(0);
        Integer resultMax = phase1.stream()
                .map(Pair::getValue1)
                .reduce(Integer::sum)
                .orElse(0);
        log.debug("Result: {}, {}", resultMin, resultMax);
    }

    private static Pair<Integer, Integer> getNextVal(List<Integer> sequence) {
        log.debug("Seq: {}", sequence);

        List<Integer> listLastSeq = new ArrayList<>();
        List<Integer> listFirstSeq = new ArrayList<>();
        listLastSeq.add(sequence.get(sequence.size() - 1));
        listFirstSeq.add(sequence.get(0));

        List<Integer> previousSeq = sequence;
        while (true) {
            List<Integer> newSeq = getNextSeq(previousSeq);
            log.debug("Next seq: {}", newSeq);

            listLastSeq.add(newSeq.get(newSeq.size() - 1));
            listFirstSeq.add(newSeq.get(0));
            if (newSeq.stream().noneMatch(e -> e != 0)) {
                break;
            }

            previousSeq = newSeq;
        }

        Integer newLastValue = listLastSeq.stream().reduce(Integer::sum).orElseThrow(RuntimeException::new);
        log.debug("new last value: {}", newLastValue);
        Collections.reverse(listFirstSeq);
        Integer newFirstValue = 0;
        for (Integer integer : listFirstSeq) {
            newFirstValue = integer - newFirstValue;
        }
        log.debug("new first value: {}", newFirstValue);

        return Pair.with(newFirstValue, newLastValue);
    }

    private static List<Integer> getNextSeq(List<Integer> sequence) {
        List<Integer> newFirstSeq = new ArrayList<>();
        Integer previous = null;
        for (Integer s : sequence) {
            if (previous != null) {
                newFirstSeq.add(s - previous);
            }
            previous = s;
        }
        return newFirstSeq;
    }
}
