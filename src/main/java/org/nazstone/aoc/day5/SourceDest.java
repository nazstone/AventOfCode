package org.nazstone.aoc.day5;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class SourceDest {
    private Integer order;
    private List<List<Long>> listInput;
    private Boolean map;
}
