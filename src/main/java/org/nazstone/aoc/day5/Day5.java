package org.nazstone.aoc.day5;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.nazstone.aoc.util.Util;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
public class Day5 {

    public static final String SEEDS = "seeds: ";

    @SneakyThrows
    public void execute() {
        log.info("Day 5");

        List<String> lines = Util.fromFileLineToStringList("./day5/input");

        List<Long> seeds = new ArrayList<>();

        Map<String, SourceDest> map = new HashMap<>();
        map.put("seed-to-soil", SourceDest.builder().map(false).order(0).build());
        map.put("soil-to-fertilizer", SourceDest.builder().map(false).order(1).build());
        map.put("fertilizer-to-water", SourceDest.builder().map(false).order(2).build());
        map.put("water-to-light", SourceDest.builder().map(false).order(3).build());
        map.put("light-to-temperature", SourceDest.builder().map(false).order(4).build());
        map.put("temperature-to-humidity", SourceDest.builder().map(false).order(5).build());
        map.put("humidity-to-location", SourceDest.builder().map(false).order(6).build());

        List<Long> tmps;

        // data init
        for (String line : lines) {
            if (line.startsWith(SEEDS)) {
                seeds = Arrays.stream(line.substring(line.indexOf(SEEDS) + SEEDS.length()).trim().split(" ")).map(Long::valueOf).toList();
                continue;
            }

            if (line.contains(":")) {
                for (Map.Entry<String, SourceDest> e : map.entrySet()) {
                    SourceDest sd = e.getValue();
                    sd.setMap(line.startsWith(e.getKey()));
                }
            } else if (line.length() > 1) {
                tmps = Arrays.stream(line.trim().split(" ")).map(Long::valueOf).toList();
                Optional<Map.Entry<String, SourceDest>> opt = map.entrySet().stream().filter(s -> s.getValue().getMap()).findFirst();
                if (opt.isPresent()) {
                    if (opt.get().getValue().getListInput() == null) {
                        opt.get().getValue().setListInput(new ArrayList<>());
                    }
                    opt.get().getValue().getListInput().add(tmps);
                }
            }
        }

        log.debug("seeds: {}", seeds);

        log.debug("map initialized");
        List<Long> result = new ArrayList<>();
        for (Long seed : seeds) {
            Long next = getEndVal(map, seed);
            result.add(next);
        }

        log.info("result: {}", result.stream().reduce(Long::min).orElse(0L));

        Long time = System.currentTimeMillis();
        Long previous = null;
        List<Pair<Long, Long>> seedsInput = new ArrayList<>();
        for (Long seed : seeds) {
            if (previous == null) {
                previous = seed;
            } else {
                seedsInput.add(Pair.with(previous, seed));
                previous = null;
            }
        }

        ExecutorService executor = Executors.newFixedThreadPool(10);
        List<Future<Long>> futures = new ArrayList<>();

        for (Pair<Long, Long> seedUnit : seedsInput) {
            Callable<Long> runnableTask = () -> {
                long min = Long.MAX_VALUE;
                for (long j = 0; j < seedUnit.getValue1(); j += 1) {
                    Long next = getEndVal(map, seedUnit.getValue0() + j);
                    min = Math.min(next, min);
                }
                log.info("end: {}", min);
                return min;
            };
            futures.add(executor.submit(runnableTask));
        }

        long min = futures.stream().map(e -> {
            try {
                return e.get();
            } catch (Exception ex) {
                return Long.MAX_VALUE;
            }
        }).reduce(Long::min).orElse(0L);
        log.info("part2 min: {}", min);
        log.info("time: {}", System.currentTimeMillis() - time);
    }

    private Long getEndVal(Map<String, SourceDest> map, Long seed) {
        log.debug("Seed {}", seed);
        Long next = seed;
        List<Map.Entry<String, SourceDest>> tmpEntries = map.entrySet().stream().sorted((e1, e2) -> e1.getValue().getOrder() - e2.getValue().getOrder()).toList();
        for (Map.Entry<String, SourceDest> tmpEntry : tmpEntries) {
            next = calculateNextValue(tmpEntry.getValue().getListInput(), next);
            log.debug("key: {} - next: {}", tmpEntry.getKey(), next);
        }
        log.debug("final: {} => {}", seed, next);
        return next;
    }

    private Long calculateNextValue(List<List<Long>> listInput, Long next) {
        for (List<Long> longs : listInput) {
            Long dest = longs.get(0);
            Long source = longs.get(1);
            Long length = longs.get(2);
            if (next >= source && next < source + length) {
                return dest + (next - source);
            }
        }

        return next;
    }
}
