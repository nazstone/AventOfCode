package org.nazstone.aoc.day19;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class Workflow {
  private String name;
  private String output;
  private List<Condition> conditions;
}
