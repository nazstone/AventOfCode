package org.nazstone.aoc.day19;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.nazstone.aoc.day18.Input;
import org.nazstone.aoc.util.Util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class Day19 {
    @Getter
    private Map<String, Workflow> workflows = new HashMap<>();
    @Getter
    private List<Map<String, Integer>> inputs = new ArrayList<>();

    @SneakyThrows
    public void execute() {
        log.info("Day 19");

        String content = Util.fromFileLineToString("day19/input");
        String[] subcontent = content.split("\\r?\\n\\r?\\n");
        initWorkflow(subcontent[0]);
        initRating(subcontent[1]);

        log.info("Part 1: {}", letsgo());
    }

    public String nextStep(Workflow workflow, Map<String, Integer> input) {
        for (Condition condition : workflow.getConditions()) {
            Integer val = input.get(condition.getProperty());
            if (condition.getCond().equals("<")) {
                if (val < condition.getVal()) {
                    return condition.getOutput();
                }
            } else {
                if (val > condition.getVal()) {
                    return condition.getOutput();
                }
            }
        }
        return workflow.getOutput();
    }

    public int letsgo() {
        int count = 0;
        for (Map<String, Integer> input : inputs) {
            String wf = "in";
            List<String> end = Arrays.asList("R", "A");
            while (!end.contains(wf)) {
                Workflow workflow = workflows.get(wf);
                wf = nextStep(workflow, input);
                log.debug("tmp: {}", wf);
            }
            log.debug("Res: {}", wf);
            if (wf.equals("A")) {
                count += input.values().stream().reduce(Integer::sum).orElse(0);
            }
        }
        return count;
    }

    public void initRating(String stmp) {
        String[] eachVal = stmp.split("\\r?\\n");
        for (String s : eachVal) {
            s = s.substring(1, s.length() - 1);
            String[] sub = s.split(",");
            Map<String, Integer> map = new HashMap<>();
            for (String s1 : sub) {
                String[] kv = s1.split("=");
                map.put(kv[0], Integer.valueOf(kv[1]));
            }
            inputs.add(map);
        }
    }

    public void initWorkflow(String s) {
        String[] lines = s.split("\\r?\\n");
        for (String line : lines) {
            int indexOf = line.indexOf("{");
            String workFlowName = line.substring(0, indexOf);

            Workflow workflow = Workflow.builder()
                    .name(workFlowName)
                    .conditions(new ArrayList<>())
                    .build();

            line = line.substring(indexOf + 1, line.indexOf("}"));
            String[] tmp = line.split(",");
            for (String s1 : tmp) {
                Pattern pattern = Pattern.compile("([xmas]{1})([<>])([0-9]+):([a-zA-Z]+)");
                Matcher match = pattern.matcher(s1);
                if (!match.find()) {
                    workflow.setOutput(s1);
                    continue;
                }
                Condition condition = Condition.builder()
                        .property(match.group(1))
                        .cond(match.group(2))
                        .val(Integer.parseInt(match.group(3)))
                        .output(match.group(4))
                        .build();
                workflow.getConditions().add(condition);
            }
            workflows.put(workflow.getName(), workflow);
        }
    }
}