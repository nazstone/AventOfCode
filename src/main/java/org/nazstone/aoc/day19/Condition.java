package org.nazstone.aoc.day19;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Condition {
  private String property;
  private String output;
  private String cond;
  private Integer val;
}
