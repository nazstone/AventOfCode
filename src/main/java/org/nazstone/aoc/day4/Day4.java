package org.nazstone.aoc.day4;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Triplet;
import org.nazstone.aoc.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class Day4 {

    @SneakyThrows
    public void execute() {
        log.info("Day 4");

        List<Triplet<String, Long, Integer>>cardsCount = new ArrayList<>();

        List<String> lines = Util.fromFileLineToStringList("./day4/input");
        Double result = lines.stream()
                .map(e -> {
                    String[] cards = e.substring(e.indexOf(":") + 1).split("\\|");
                    List<String> inputTmp = Arrays.stream(cards[0].trim().split("\s+")).toList();
                    List<Integer> winningTmp = Arrays.stream(cards[1].trim().split("\s+")).toList()
                            .stream().map(Integer::valueOf).toList();

                    long countWinning = inputTmp.stream().map(Integer::valueOf).filter(winningTmp::contains).count();

                    double val = countWinning == 0 ? 0 : Math.pow(2, countWinning - 1);
                    log.debug("{}: {} = {}", e.substring(0, e.indexOf(":")), countWinning, val);

                    cardsCount.add(Triplet.with(e.substring(0, e.indexOf(":")), countWinning, 1));

                    return val;
                })
                .reduce(Double::sum)
                .orElse((double) 0);
        log.info("Result: {}", result);

        for (int i = 0; i < cardsCount.size(); i++) {
            long val = cardsCount.get(i).getValue1();
            for (int j = 1; j <= val; j += 1) {
                if (cardsCount.size() > i + j) {
                  int recount = (cardsCount.get(i).getValue2());
                  Triplet<String, Long, Integer> triplet = cardsCount.get(i + j);
                  cardsCount.remove(triplet);
                  cardsCount.add(i + j, Triplet.with(triplet.getValue0(), triplet.getValue1(), recount + triplet.getValue2()));
                }
            }
        }
        log.info("part 2: {}", cardsCount.stream().map(Triplet::getValue2).reduce(Integer::sum).orElse(0));
    }
}
